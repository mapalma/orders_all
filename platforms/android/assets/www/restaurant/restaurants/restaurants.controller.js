(function() {
  'use strict';

  angular
    .module('starter')
    .controller('RestaurantsController', RestaurantsController);

  /** @ngInject */
  function RestaurantsController($scope, $state, $location, $ionicHistory, StorageService, RestaurantService, $ionicSideMenuDelegate, $ionicLoading) {
    var vm = this;

    vm.recargarRestaurants = recargarRestaurants;
    vm.loadRestaurant = loadRestaurant;

    function getAllRestaurants () {
      //conexionintersuper(); // verificar la conexion a internet
      vm.isInternet = true;
      if (vm.isInternet) {
        console.log("ingreso");
        //showLoading($ionicLoading);
        RestaurantService.getAllRestaurants(function(response) {
            vm.restaurants = response.data;
            console.log('los restaurants son ' , vm.restaurants);
            //hideLoaging($ionicLoading);
        });
      }
    }

    function recargarRestaurants() {
      getAllRestaurants();
      verifyInternet();
      vm.$broadcast('scroll.refreshComplete');
    };

    function loadRestaurant(restaurant) {
      console.log("El restaurant selected is " , restaurant);
      vm.restaurantSelected = restaurant;
      RestaurantService.setRestaurant(restaurant);
      //load data of paypal
      shopPaypal.initialize();
    }

    function verifyInternet() {
      var networkState = navigator.connection.type;
      if (networkState === Connection.UNKNOWN || networkState === Connection.NONE) {
          $scope.isInternet = false;
      } else {
          $scope.isInternet = true;
      }
    }

    //load by default
    getAllRestaurants();

    // function cargarSupermercado(bd_name, _promocion, _ubicacion, _listas, _id_paypal, _pagar_entrega) {
    //   if (!_pagar_entrega) {
    //     _pagar_entrega = 0.0
    //   }
    //
    //   StorageService.removeAll();
    //   StorageService.add({bd : bd_name,
    //                       promocion: _promocion,
    //                       ubicacion: _ubicacion,
    //                       listas: _listas,
    //                       id_paypal: _id_paypal,
    //                       pagar_entrega: _pagar_entrega});
    //    //cargar id para paypal
    //   if (_id_paypal) {
    //     id_paypal = _id_paypal;
    //     lenguaje = $translate.use();
    //     shopPaypal.initialize();
    //   }
    // }
////////////////////////
  }
})();
