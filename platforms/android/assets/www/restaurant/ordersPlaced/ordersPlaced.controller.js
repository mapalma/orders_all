(function() {
  'use strict';

  angular
    .module('starter')
    .controller('OrdersController', OrdersController);

  /** @ngInject */
  function OrdersController(OrderPlaceService, MapService) {
    var vm = this;
    vm.setOrder = setOrder;
    vm.loadData = loadData;

    function getOrdersPlaced() {
    	OrderPlaceService.getAllordersPlaced(function(response){
    		vm.ordersPlaced = response;
        filterOrders();
    	});
    }

    function loadData(data) {
      if (data === 'activos') {
        vm.filterOrdersPlaced = vm.ordersActive;
      } else {
        vm.filterOrdersPlaced = vm.delivered;
      }
    }

    function filterOrders() {
      vm.ordersActive = [];
      vm.delivered = [];
      angular.forEach(vm.ordersPlaced, function(value, key) {
        if (value.estado === 'PREPARANDO' || value.estado === 'ENVIANDO') {
          vm.ordersActive.push(value);
        } else {
          vm.delivered.push(value);
        }
      });
      vm.filterOrdersPlaced = vm.ordersActive;
    }

    function setOrder(order) {
      OrderPlaceService.setOrder(order);
      MapService.setPosition(order.coordenadas);
      loadMap();
    }

    function loadMap() {
      console.log("cargar mapa");
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDzzRp2LaRXcRUEBO2T1uIfLb7tOk58JAM';
      document.body.appendChild(script);
    }


	   getOrdersPlaced();

  }
})();
