angular.module('googlemaps.init', [])

.config(['uiGmapGoogleMapApiProvider', function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyDzzRp2LaRXcRUEBO2T1uIfLb7tOk58JAM',
        libraries: 'weather,geometry,visualization'
    });
}])

.directive('creatorMapComponent', ['uiGmapGoogleMapApi', '$timeout',

    /*
        Do not remove this directive, it is what powers the Creator Drag & Drop Component.
    */

    function(uiGmapGoogleMapApi, $timeout) {

        return {
            restrict: 'E',
            scope: true,
            link: function($scope, $element, $attr, _c, transclude) {
            
                $scope.map = {};
                
                if ($attr.marker=="true"){
                    $scope.map.marker = {
                        id: 0
                    }
                }
                
                $attr.$observe('location',function(val){
                    
                        uiGmapGoogleMapApi.then(function(maps){

                            function setupMap(lat, lng){
                                
                                $scope.map.zoom = parseInt($attr.zoom);
                                $timeout(function(){
                                    $scope.map.center = {
                                        latitude: lat,
                                        longitude: lng
                                    };  
                                });
                                $scope.map.options = JSON.parse($attr.options);
                                
                                if ($attr.marker=="true"){
                                    $scope.map.marker.coords = {
                                        latitude: lat,
                                        longitude: lng
                                    }
                                }
                            }

                                var directionsDisplay = new google.maps.DirectionsRenderer();
                                var directionsService = new google.maps.DirectionsService();
                                var geocoder = new google.maps.Geocoder();
                                


                                // directions object -- with defaults
                                
                                 
                                

                                 // get directions using google maps api
                                 function getDirections (origin,destination) {
                                        var request = {
                                        origin: origin,
                                        destination: destination,
                                        travelMode: google.maps.DirectionsTravelMode.DRIVING,
                                        
                                       };
                                        directionsService.route(request, function (response, status) {
                                               if (status === google.maps.DirectionsStatus.OK) {
                                                directionsDisplay.setDirections(response);
                                                directionsDisplay.setMap($scope.map.control.getGMap());
                                                directionsDisplay.setPanel(document.getElementById('directionsList'));
                                                $scope.directions.showList = true;



                                              } else {
                                                alert('Google route unsuccesfull!');
                                              }
                                        });
                                        

                                   }
                                   
                            if (val.indexOf('"latitude"') > -1){
                                val = JSON.parse(val);
                                setupMap(val.latitude, val.longitude);
                                
                                //getDirections({$scope.clilati,$scope.clilongi},{val.latitude, val.longitude})

                            }else{
                                var geocoder = new maps.Geocoder();
                                geocoder.geocode({'address' : val}, function(results, status){
                                    
                                    $scope.$apply(function(){
                                        setupMap(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                                       // getDirections(results[0].geometry.location.lat(), results[0].geometry.location.lng(),location1);
                                     });
                
                                 });
                                
                             }
                            
                         
                         });
                           
                 });

                
            
             }
      
         };
    }   
]);