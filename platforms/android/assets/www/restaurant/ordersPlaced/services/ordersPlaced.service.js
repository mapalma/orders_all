(function() {
  'use strict';

  angular
    .module('starter')
    .factory('OrderPlaceService', OrderPlaceService);

    function OrderPlaceService($http, StorageService) {
        var service = {};
        var orderPlaceSelected;
        var ordersPlace = 'undefined';

        var usuario = StorageService.getUser();
        var user = usuario[0];
        var _id = user[0].id;

        service.getAllordersPlaced = function (callback) {
              $http.get(urlws + '/ordersPlaced/' + _id)
              .success(function (response) {
                callback(response.data);
                ordersPlace = response.data;
              }).
              error(function(data) {
                console.log('error get all ordersPlace');
              });
        };

        service.setOrder = function (order) {
          orderPlaceSelected = order;
        }

        service.getOrderSelected = function (callback) {
          callback(orderPlaceSelected);
        }

        return service;
    }
})();
