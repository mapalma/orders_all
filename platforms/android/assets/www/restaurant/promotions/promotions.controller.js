(function() {
  'use strict';

  angular
    .module('starter')
    .controller('PromotionsController', PromotionsController);

  /** @ngInject */
  function PromotionsController(RestaurantService) {
    var vm = this;
    vm.getAllPromotions = getAllPromotions;
    vm.numberPromotions = 0;

    function getAllPromotions() {
      vm.isInternet = true;
      if (vm.isInternet) {
        RestaurantService.getAllRestaurants(function(response) {
            vm.restaurants = response.data;
            vm.numberPromotions = vm.restaurants.length;
        });
      }
    }

    //load by default
    getAllPromotions();
  }
})();
