(function() {
  'use strict';

  angular
    .module('starter')
    .factory('RestaurantService', RestaurantService);

    function RestaurantService($http) {
        var service = {};
        var restaurantSelected;
        var restaurants = 'undefined';

        service.getAllRestaurants = function (callback) {
            if (restaurants !== 'undefined') {
              console.log("no llamo el endpoint");
              callback(restaurants);

            } else {
              console.log("llamo al endpoint");
              $http.get(urlws + '/restaurants')
              .success(function (response) {
                callback(response);
                restaurants = response;
              }).
              error(function(data) {
                console.log('error get all restaurants');
              });
            }
        };

        service.setRestaurant = function (restaurant) {
          restaurantSelected = restaurant;
        }

        service.getRestaurantSelected = function (callback) {
          callback(restaurantSelected);
        }

        return service;
    }
})();
