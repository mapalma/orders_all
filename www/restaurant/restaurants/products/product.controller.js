(function() {
  'use strict';

  angular
    .module('starter')
    .controller('ProductController', ProductController);

  /** @ngInject */
  function ProductController(ProductService, PedidoService, $state) {
    var vm = this;
    vm.loadProductSelected = loadProductSelected;
    vm.addCar = addCar;

    function loadProductSelected() {
      ProductService.getProductSelected(function(response) {
          vm.productSelect = response;
          console.log("el producto es desde el reponse " , vm.productSelect);
      });
    }

    function addCar(product) {
      PedidoService.addProduct(product);
      console.log("producto agregago");
    }

    //load data
    loadProductSelected();
  }

})();
