(function() {
  'use strict';

  angular
    .module('starter')
    .controller('OrdersDetailController', OrdersDetailController);

  /** @ngInject */
  function OrdersDetailController(OrderPlaceService) {
    var vm = this;

    function getOrderSelected() {
      OrderPlaceService.getOrderSelected(function (order) {
        vm.orderSelected = order;
        console.log("las ordenes seleccionadas son " , vm.orderSelected);
        vm.productsOrders = angular.fromJson(vm.orderSelected.json);
        console.log("la seccion dos es " ,vm.productsOrders );
        calculateSubTotal();
      });
    }

    function calculateSubTotal() {
      vm.subtotal = 0;
      angular.forEach(vm.productsOrders, function(value, key) {
        vm.subtotal += (value.cantidad * value.valor);
      });
      calculateTotal();
    }

    function calculateTotal() {
      console.log("subtotal " , vm.subtotal);
      console.log("costo envio " ,  vm.orderSelected.costo_envio);
      vm.total = (parseInt(vm.subtotal) + parseInt(vm.orderSelected.costo_envio));
    }

    //load data
	   getOrderSelected();
  }
})();
