var http ;
var dataPedido = [];
var $translate;
var state;
var PedidoService;


var shopPaypal = {
  // shopPaypallication Constructor
  initialize: function() {
    this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicity call 'shopPaypal.receivedEvent(...);'
  onDeviceReady: function() {
    shopPaypal.receivedEvent('deviceready');
  },
  // Update DOM on a Received Event
  receivedEvent: function(id) {
    // start to initialize PayPalMobile library
    shopPaypal.initPaymentUI();
  },
  initPaymentUI: function() {
    var clientIDs = {
      "PayPalEnvironmentProduction": "YOUR_PRODUCTION_CLIENT_ID",
      "PayPalEnvironmentSandbox": "AXI9eUUu0gZ4DodY_vBDXSOltpuW-gvcfyacBy1Z2Vl2m5sswdDcqxh62FsG4HhjSeppM1ZtHmg1BH_L"
    };
    PayPalMobile.init(clientIDs, shopPaypal.onPayPalMobileInit);

  },
  onSuccesfulPayment: function(payment) {
    console.log("payment success: " + JSON.stringify(payment, null, 4));
    console.log(">>: response" + payment.response.state);
      if (payment.response.state === 'approved') {

          shopPaypal.enviarListaSupermercado();
          shopPaypal.mensaje();
      }
  },
  mensaje : function(){
    $translate(['pagadacorrecta']).then(function (translations) {
      window.plugins.toast.showShortBottom(translations.pagadacorrecta + ". !", function (a) {
          console.log('toast success: ' + a)
      }, function (b) {
          alert('toast error: ' + b)
      });
    })
  },
  enviarListaSupermercado : function(){
      console.log("es hora de enviar el pedido");

        var url = urlws + "/saveOrders";
        console.log("se va a enviar " , dataPedido);
        http.post(url, dataPedido)
          .then(function(res) {
            //callback(res);
            console.log("destruir pedido");
            PedidoService.destroyOrder();
            state.go("restaurants");
          }, function(err){
            console.log("error al enviar el pedido");
          });

  },

  onAuthorizationCallback: function(authorization) {
    console.log("authorization: " + JSON.stringify(authorization, null, 4));
  },
  createPayment: function(dolares) {
    // for simplicity use predefined amount
    var paymentDetails = new PayPalPaymentDetails(dolares, "0.00", "0.00");
    /*var payment = new PayPalPayment("50.00", "USD", "Awesome Sauce", "Sale",
      paymentDetails);*/
    var payment = new PayPalPayment(dolares, "USD", "Pedido", "Sale",
      paymentDetails);
    return payment;
  },
  configuration: function() {
    // for more options see `paypal-mobile-js-helper.js`
    var config = new PayPalConfiguration({
      merchantName: "Shop-List",
      merchantPrivacyPolicyURL: "https://mytestshop.com/policy",
      merchantUserAgreementURL: "https://mytestshop.com/agreement",
      languageOrLocale: "es"
    });
    return config;
  },

  comprar: function(dolares, data, $http, _$translate, _$state, _PedidoService) {
    //load data of lista to send server
    dataPedido = data;
    http      = $http;
    $translate = _$translate;
    state = _$state;
    PedidoService = _PedidoService;
  // single payment
  PayPalMobile.renderSinglePaymentUI(
    shopPaypal.createPayment(dolares),
    shopPaypal.onSuccesfulPayment,
    shopPaypal.onUserCanceled);
  },

  onPrepareRender: function() {
    // buttons defined in index.html
    //  <button id="buyNowBtn"> Buy Now !</button>
    //  <button id="buyInFutureBtn"> Pay in Future !</button>
    //  <button id="profileSharingBtn"> ProfileSharing !</button>
    var buyNowBtn = document.getElementById("buyNowBtn");
    var buyInFutureBtn = document.getElementById("buyInFutureBtn");
    var profileSharingBtn = document.getElementById("profileSharingBtn");
  },
  onPayPalMobileInit: function() {
    // must be called
    // use PayPalEnvironmentNoNetwork mode to get look and feel of the flow
    /*PayPalMobile.prepareToRender("PayPalEnvironmentNoNetwork", shopPaypal.configuration(),
      shopPaypal.onPrepareRender);*/
    PayPalMobile.prepareToRender("PayPalEnvironmentSandbox", shopPaypal.configuration(),
      shopPaypal.onPrepareRender);
  },
  onUserCanceled: function(result) {
    console.log(result);
  }
};
