//angular.module('angular.module('my.controllers', [])')
angular.module('starter')
    .controller('recuperarCtrl', function ($scope,
                                            $http,
                                            $state,
                                            StorageService,
                                            $ionicPopup,
                                            $ionicLoading) {
    $scope.verificarCorreo      = verificarCorreo;
    $scope.verificarCodigo      = verificarCodigo;  
    $scope.actualizarContrasenia= actualizarContrasenia;  
    $scope.cancelar             = cancelar;
    $scope.formRecuperar        = true;

    function verificarCorreo (recuperar) {
         showLoading($ionicLoading);
        //paso 1 verificar si correo existe .. si existe enviar un correo con un codigo 
        var url = urlws + "/recuperar/clave/cliente";
        $http.post(url, recuperar).success(function (respuesta) {
            hideLoaging($ionicLoading);
            if (respuesta.status === 200) {
                $scope.formCodigo = true;
                $scope.formRecuperar = false;
                $scope.id = respuesta.data[0].id;
                showAlert($ionicPopup);
            } else {
                $scope.formCodigo = false;
                showAlertError($ionicPopup);
            }
        }).error(function (data, status, headers, config) {
            hideLoaging($ionicLoading);
            $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
            console.log('-->' + $scope.token);
        });
    }

    function verificarCodigo(recuperar) {
        showLoading($ionicLoading);
        var url = urlws + "/recuperar/clave/cliente";
        $http.put(url, recuperar).success(function (respuesta) {
            hideLoaging($ionicLoading);
            if (respuesta.status === 200) {
                $scope.formActualizar = true;
                $scope.formCodigo = false;
            } else {
                $scope.formCodigo = true;
                mensajePopup($ionicPopup, 'Recuperar Contraseña' , 'El código ingresado es ínvalido');
            }
        }).error(function (data, status, headers, config) {
            hideLoaging($ionicLoading);
            $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
            console.log('-->' + $scope.token);
        });
    }

    function actualizarContrasenia(recuperar) {
        showLoading($ionicLoading);
        var url = urlws + "/actualizar/clave/cliente/" + $scope.id;
        $http.post(url, recuperar).success(function (respuesta) {
            hideLoaging($ionicLoading);
           
        }).error(function (data, status, headers, config) {
            hideLoaging($ionicLoading);
            $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
            console.log('-->' + $scope.token);
        });
    }

    function cancelar (){
        $state.go('login');
        $scope.formCodigo       = false;
        $scope.formRecuperar    = false;
        $scope.formActualizar   = false;
    }

});


showAlert = function($ionicPopup) {
   var alertPopup = $ionicPopup.alert({
        title: 'Recuperar Contraseña',
        template: 'El codigo a sido enviado al correo !'
   });
   alertPopup.then(function(res) {
        console.log('Thank you for not eating my delicious ice cream cone');
   });
 };

showAlertError = function($ionicPopup) {
   var alertPopup = $ionicPopup.alert({
        title: 'Recuperar Contraseña',
        template: 'Correo no registrado'
   });
   alertPopup.then(function(res) {
        console.log('Thank you for not eating my delicious ice cream cone');
   });
 };

mensajePopup = function($ionicPopup, titulo, contenido) {
    var alertPopup = $ionicPopup.alert({
        title: titulo,
        template: contenido
   });
   alertPopup.then(function(res) {
     console.log('Thank you for not eating my delicious ice cream cone');
   });
 };

//metodos de loading
showLoading = function($ionicLoading) {
    $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
};

hideLoaging = function ($ionicLoading){
    $ionicLoading.hide();
};
