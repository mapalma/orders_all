//angular.module('angular.module('my.controllers', [])')
angular.module('starter')
.controller('ManualCtrl', function ($scope, 
                                    $translate) {

    $scope.verInicio    		= verInicio;
    $scope.verSupermercados    	= verSupermercados;
    $scope.verListas    		= verListas;
    $scope.verPerfil    		= verPerfil;

    $scope.inicio       		= false;
    $scope.supermercados		= false;
    $scope.listas       		= false;
    $scope.perfil       		= false;

    function verInicio() {
        $scope.inicio = $scope.inicio === true ? false : true;
    }

    function verSupermercados() {
        $scope.supermercados = $scope.supermercados === true ? false : true;
    }

    function verListas() {
        $scope.listas = $scope.listas === true ? false : true;
    }

    function verPerfil() {
        $scope.perfil = $scope.perfil === true ? false : true;
    }

});
