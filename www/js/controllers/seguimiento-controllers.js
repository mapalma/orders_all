angular.module('starter')
	.controller('SeguimientoListaCtrl', function ($scope, 
			                                    $http, 
			                                    $state,
			                                    StorageService,
			                                    listasService,
			                                    $stateParams,
			                                    $translate) {

	$scope.goSeguimiento 		= goSeguimiento;
	var estado;
	var lista_result;

	function goSeguimiento(lista) {
		if (lista.id_lista_super) {
			$state.go('seguimiento',{"listaId" : lista.id_lista_super});	
		} else {
            $translate(['pagarLista']).then(function (translations) {
                window.plugins.toast.showShortBottom(translations.pagarLista + ". !", function (a) {
                    console.log('toast success: ' + a)
                }, function (b) {
                    alert('toast error: ' + b)
                });
            });
		}
	}

	$scope.cargarEstadolista = function(){
		var listaId = $stateParams.listaId;
		var bd_super = StorageService.getBd();
		listasService.doEstado(listaId, bd_super).then(function(_lista) {
            cargarColores(_lista);
        });
	}

	function cargarColores(lista){
		if (lista.estado_inicial === 1){
			$scope.estado_inicial 		= true;
			$scope.estado_proceso 		= false;
			$scope.estado_despachada 	= false;
		} else if (lista.estado_proceso === 1){
			$scope.estado_inicial 		= false;
			$scope.estado_proceso 		= true;
			$scope.estado_despachada 	= false;
		} else if (lista.estado_despachada === 1){
			$scope.estado_inicial 		= false;
			$scope.estado_proceso 		= false;
			$scope.estado_despachada 	= true;
		}
	}
});
