// Ionic Starter App

var listaResultado = [];
litasMetodos = function (listaLLegada) {
    listaResultado = listaLLegada;
};

var db = null;
var inciarListas = [];
var app = angular.module('starter', ['ionic',
                                         'ngCordova',
                                         'ngStorage',
                                         'starter.controllers',
                                         'loginController',
                                         'generalController',
                                         'pascalprecht.translate'])
    /*inicia la app*/
    .run(function ($ionicPlatform,
                   $cordovaSQLite,
                   $rootScope,
                   StorageService,
                   $location,
                   listasService) {

        $ionicPlatform.ready(function () {

            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }

            /*tamanio de la base de datos*/
            var dbSize = 5 * 1024 * 1024; // 5MB

            if (window.cordova) {
               // db = $cordovaSQLite.openDB({name: "shopbd.db"}); //device
               db = $cordovaSQLite.openDB( 'shopbd.db', 1 );
            } else {
                /*creamos bd*/
                //db = window.openDatabase("my.db", '1', 'my', 1024 * 1024 * 100); // browser
                db = window.openDatabase("my.db", "1.0", "Córdoba Demo", 200.000); //para chrome
            }
            /*creamos las tablas*/
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS listasrk(ID INTEGER PRIMARY KEY ASC, nombre VARCHAR, precio FLOAT, bd_super VARCHAR, fecha DATE, id_lista_super INTEGER)");
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS productosrk(ID INTEGER PRIMARY KEY ASC, nombre VARCHAR, codigo INTEGER, precio FLOAT, id_lista INTEGER,  imagen VARCHAR, cantidad INTEGER , precioTotal FLOAT, ubicacion_id VARCHAR , estado BOOLEAN , comprado BOOLEAN)");

            var arreglo = StorageService.getUser();
            //var usuario = arreglo[0];

            if(arreglo.length > 0) {
                $location.path('/restaurants');
                //cargar los supermercados
                listasService.doSupermercados().then(function(respuesta) {
                    console.log("los supermercados al cargar al incio son " , respuesta);
                });
            }else {

                //$location.path('/login');
                //$state.go('login');
            }
        });
    })
        /*configuracion*/
        .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $translateProvider) {
            $ionicConfigProvider.views.maxCache(0); //en caso de cambiar valores a la vista que se actulicen automaticamente

            //para el idioma
            for (lang in translations) {
                $translateProvider.translations(lang, translations[lang]);
            }

            $translateProvider.preferredLanguage('es');

            $stateProvider
                    // navegation
                    .state('supermercados', {
                        url: '/supermercados',
                        templateUrl: 'templates/super/tab-super.html',
                        controller: 'SupermercadoCtrl'
                    })
                    .state('listas', {
                        url: '/listas',
                        templateUrl: 'templates/tab-listas.html',
                        controller: 'listasTableCtrl'
                    })
                    .state('listaseleccionada', {
                        url: '/listselect/:listaId',
                        templateUrl: 'templates/productos.html',
                        controller: 'productosCtrl'
                    })
                    .state('seguimiento', {
                        url: '/listselect/seguimiento/:listaId',
                        templateUrl: 'templates/seguimiento.html',
                        controller: 'SeguimientoListaCtrl'
                    })
                    .state('productoslist', {
                        url: '/productoslist/:productoId',
                        templateUrl: 'templates/pro-detail.html',
                        controller: 'prod_detail_Ctrl'
                    })
                   .state('promociones', {
                        url: '/promociones',
                        templateUrl: 'templates/tab-promociones.html',
                        controller: 'promocionesCtrl'
                    })
                    .state('ubicacionp', {
                        url: '/ubicacionp',
                        templateUrl: 'templates/tab-ubicacion.html',
                        controller: 'ubicacionCtrl'
                    })
                    .state('bienvenido', {
                        url: '/bienvenido',
                        templateUrl: 'templates/bienvenido.html'

                    })
                    .state('ubi-supermercado', {
                        url: '/ubi-supermercado',
                        templateUrl: 'templates/ubi-supermercado.html',
                        controller: 'ubisupermercadoCtrl'
                    })
                    //login
                    .state('login', {
                        url: '/login',
                        templateUrl: 'views/login.html',
                        controller: 'loginCtrl'
                    })
                    .state('registrarse', {
                        url: '/registrarse',
                        templateUrl: 'views/registrarse.html',
                        controller: 'registrarseCtrl'
                    })
                    .state('recuperarclave', {
                        url: '/recuperarclave',
                        templateUrl: 'views/recuperar-clave.html',
                        controller: 'recuperarCtrl'
                    })
                    //user
                    .state('perfil', {
                        url: '/perfil',
                        templateUrl: 'templates/perfil.html',
                        controller: 'PerfilCtrl'
                    })
                    .state('manual', {
                        url: '/manual',
                        templateUrl: 'templates/manual.html',
                        controller : 'ManualCtrl'
                    })
                    //====================================//
                    //         RESTAURANTS                //
                    //====================================//
                    .state('restaurants', {
                        url: '/restaurants',
                        templateUrl: 'restaurant/restaurants/restaurants.view.html',
                        controller : 'RestaurantsController as vm'
                    })
                    .state('promotions', {
                        url: '/promotios',
                        templateUrl: 'restaurant/promotions/promotions.view.html',
                        controller : 'PromotionsController as vm'
                    })
                    .state('restaurant', {
                        url: '/restaurantSelected',
                        templateUrl: 'restaurant/restaurants/restaurant/restaurant.view.html',
                        controller : 'RestaurantController as vm'
                    })
                    .state('restaurantMenu', {
                        url: '/restaurantMenu',
                        templateUrl: 'restaurant/restaurants/restaurant/menu.view.html',
                        controller : 'RestaurantController as vm'
                    })
                    .state('detailProduct', {
                        url: '/detailProduct',
                        templateUrl: 'restaurant/restaurants/products/product.view.html',
                        controller : 'ProductController as vm'
                    })
                    .state('pedidos', {
                        url: '/pedidos',
                        templateUrl: 'restaurant/restaurants/pedidos/pedido.view.html',
                        controller : 'PedidoController as vm'
                    })
                    .state('ordersPlaced', {
                        url: '/ordersPlaced',
                        templateUrl: 'restaurant/ordersPlaced/ordersPlaced.view.html',
                        controller : 'OrdersController as vm'
                    })
                    .state('map', {
                        url: '/map',
                        templateUrl: 'restaurant/ordersPlaced/mapa/mapapedido.html',
                        controller : 'MapController as vm'
                    })
                    .state('orderDetail', {
                        url: '/orderDetail',
                        templateUrl: 'restaurant/ordersPlaced/orderDetail/orderDetail.view.html',
                        controller : 'OrdersDetailController as vm'
                    })

            // al iniciar la app se cargara la url bienvenido
            $urlRouterProvider.otherwise('/login');

        })
        // create a new factory
        .factory ('StorageService', function ($localStorage) {
            //local store para el usuario
            $localStorage.user = [];
            var _saveUser = function (user) {
              $localStorage.user.push(user);
            }
            var _getUser = function () {
              return $localStorage.user;
            };
            var _deleteUser = function () {
              return $localStorage.user =[];
            };

            $localStorage = $localStorage.$default({
                things: []
            });

            var _getAll = function () {
              return $localStorage.things;
            };

            var _getBd = function () {
              return $localStorage.things[0].bd;
            };
            var _getPaypal = function () {
              return $localStorage.things[0].id_paypal;
            };
            var _pagar_entrega = function () {
              return $localStorage.things[0].pagar_entrega;
            };

            var _add = function (thing) {
              $localStorage.things.push(thing);
            }

            var _remove = function (thing) {
              $localStorage.things.splice($localStorage.things.indexOf(thing), 1);
            }
            var _removeAll = function (thing) {
              $localStorage.things = [];
            }

            return {
                getAll      : _getAll,
                add         : _add,
                remove      : _remove,
                removeAll   : _removeAll,
                getBd       : _getBd,
                saveUser    : _saveUser,
                getUser     : _getUser,
                deleteUSer  : _deleteUser,
                getPaypal   : _getPaypal,
                pagar_entrega:_pagar_entrega

              };
        });
